# Test task

The project performs the following functions:

We have 2 types of clients:

Advertisers and clients.

Advertiser can create ad campaign with banner.
On the main page of the site you can see code (it works for all advertisers).

This is formal code, url starts from the site root.

To test the display of the banner you need to go to http://localhost:3000/get_banner.

On this page http://localhost:3000/get_banner you can see banner for one of advertisers.
Banner was showed -  we increase impressions for related campaign. 
If we click then we increase clicks for related campaign.
Pictures appear randomly.

We can see data updates for impressions, clicks, and conversion in real time on campaign's page (without reloading, without ajax).

For auto-update, you must run the console in the project directory and run:
rackup danthes.ru -s thin -E production
Without launching danthes we will see error on http://localhost:3000/get_banner !

---

# Это результат выполнения небольшого тестового задания.


Проект выполняет следующую функцию:

У нашего сайта есть 2 типа клиентов:

- рекламодатели
- те кто размещают наш код вставки и показывают у себя на сайте баннер наших рекламодателей

Сделан простой функционал именно для рекламодателей.

Вы можете создать свою рекламную кампанию.

Вы добавляете при создании картинку.

На главной странице сайта есть код вставки - код формальный, в нем фрейм с url от корня сайта.

(Чтобы тестировать показ баннера заходим на http://localhost:3000/get_banner)

Партнеры нашего сайта размещают у себя на сайте код вставки.

Заходя на http://localhost:3000/get_banner мы видим, что будет отображаться на сайте наших партнеров испоьзующих наш код вставки.

Мы обновляем страницу и таким образом добавляем показы той кампании чья картинке отобразилась.
Если мы кликнем то добавим клик.
Картинки берутся рандомно.

Данные по показам, кликам и конверсия - то есть процент переходов(кликов) к показам обновляются в режиме реального времени на странице просмотра рекламной кампании
 без перезагрузки страницы.

Для автообновления необходимо запустить в консоли в директории проекта

rackup danthes.ru -s thin -E production

Без запуска danthes страница просмотра баннера(http://localhost:3000/get_banner) выдаст ошибку!!!



Запуск тестов -  здесь первое знакомство c тестами.

rspec ./spec/features/test_spec.rb

Используется simplecov для информации по тестированию.

В проекте также используется weinre для дебаггинга верстки и js на любой ОС при просмотре сайта на любом мобильном устройстве. Но если вы не знаете как оно работает то проверить это не получится.