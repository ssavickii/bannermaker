require 'spec_helper'

require 'simplecov'
SimpleCov.start

#Capybara.default_driver = :selenium

describe "Test" do

  before(:each) do
    @campaign = FactoryGirl.create(:campaign)
  end

  it "GET BANNER PAGE" do

    1.times do
      visit('/get_banner')
    end

    expect(page).to have_selector 'a'
    expect(page).to have_selector 'img'

  end

  it 'visit MAIN PAGE and click link New campaign' do
    visit('/')
    expect(page).to have_content 'Make your banner!'

    click_link 'Make your banner!'
    expect(page).to have_content 'New campaign'
  end

  it 'SHOW CAMPAIGN' do
    visit('/campaigns')
    find('tr#1').click_link('Show')
    expect(find('h2')).to have_content 'test name'
  end

end

describe GetBannerController, :type => :controller do
  it 'get random campaign' do
    get :index, use_route: :get_banner
    response.should render_template :index
  end
end

describe CampaignsController, :type => :controller do
  it 'CREATE NEW CAMPAIGN' do
    campaign = FactoryGirl.create(:campaign)
    campaign.save
  end
end

describe 'LOAD_TEST' do
  it 'LOAD_TEST' do
    system "ab -c 10 -n 100 http://localhost:3000/get_banner"
  end
end
